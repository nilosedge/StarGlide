import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Random;

public class StarGlide extends JFrame {

		private static class Point {
			public double x;
			public double y;
			public Point(double x, double y) {
				this.x = x; this.y = y;
			}
		}

		private Point[] starField;
		private static final int NUM = 10000;
		private static final int MAX_X = 1400/2;
		private static final int MAX_Y = 1050/2;
		private static final int MAX_SCALE = 1000;
		private static final double SCALE_FACTOR = 1.01;
		private static final double THETA = 4 * Math.PI/180;
		private static final double COS_5 = Math.cos(THETA);
		private static final double SIN_5 = Math.sin(THETA);

		public StarGlide() {
			super("Star Glide");
			starField = initializeStars();
			JPanel contentPane = new JPanel() {
				private void plot(Graphics g, Point pt) {
					g.drawRect((int) Math.round(pt.x), (int) Math.round(pt.y), 0, 0);
	
				}
				public void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.translate(MAX_X/2, MAX_Y/2);
					g.setColor(Color.white);
					for (int i = 0; i < starField.length; i++) {
						plot(g, starField[i]);
					}
					g.translate(-MAX_X/2, -MAX_Y/2);
				}
			};
			contentPane.setBackground(Color.black);
			JButton button = new JButton("Press Me");

			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					new Thread(new Runnable() {
						public void run() {
							for(int i = 0; i < MAX_SCALE; i++) {
								scale(starField);
								rotate(starField);
								repaint();
								try {
									Thread.sleep(100);
								} catch (InterruptedException ex) { }
							}
						}
					}).start();
				}
			});

			contentPane.setLayout(new FlowLayout());
			contentPane.add(button);
			setContentPane(contentPane);
			setSize(MAX_X, MAX_Y);

			addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent ev) {
					System.exit(0);
				}
			});

			setVisible(true);
		}


		private static void scale(Point[] pts) {
			for(int i = 0; i < pts.length; i++) {
				pts[i].x *= SCALE_FACTOR;
				pts[i].y *= SCALE_FACTOR;
			}
		}

		private static void rotate(Point[] pts) {
			for(int i = 0; i < pts.length; i++) {
				double x = pts[i].x;
				double y = pts[i].y;
				pts[i].x = x * COS_5 - y * SIN_5;
				pts[i].y = x * SIN_5 + y * COS_5; 
			}
		}

		private static Point[] initializeStars() {
			Point[] result = new Point[NUM];
			Random random  = new Random();
			for (int i = 0; i < result.length; i++) {
				result[i] = new Point(random.nextInt(MAX_X) - MAX_X/2, random.nextInt(MAX_Y) - MAX_Y /2);
			}
			return result;
		}

		public static void main(String[] args) {
			StarGlide window = new StarGlide();
		}
}
